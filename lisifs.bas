Function LISTIFS(rng As Range, oddelovac As String, _
    ParamArray arguments() As Variant) As String
    'polePodminek1 As Range, podminka1 As String, _
    'Optional polePodminek2 As Range, Optional podminka2 As String, _
    'Optional polePodminek3 As Range, Optional podminka3 As String) As String
    'LISTIFS ( value_range , separator ,  criteria_range1 , criteria1 , [critera_range2 , criteria2]...)
    'https://www.reddit.com/r/excelevator/comments/8vfoag/udf_maxifs_min_range_criteria_range1_criteria1/
    Application.ScreenUpdating = False
    
    Dim lng As Integer
    lng = rng.Count
    
    Dim matches() As String
    ReDim matches(lng)
    Dim matchCount As Integer
    matchCount = 0
    ' get matches
    Dim arg As Integer, argCnt As Integer, l As Integer
    Dim cell As Variant
    Dim compchars As String

    uB = UBound(arguments)
    argCnt = uB  ' - 1
    Dim crit As String
    
    Dim i As Integer
    For i = 1 To lng
        Dim match As Boolean
        match = True
        For arg = 0 To argCnt Step 2
            Dim x As Variant
            x = arguments(arg)
            cell = x(i, 1)
            crit = CStr(arguments(arg + 1))  ' expecting string or number
            If Mid(crit, 1, 1) = "=" Or Mid(crit, 1, 1) = "<" Or Mid(crit, 1, 1) = ">" Then
                ' added eval type
                Dim expression As String
                expression = """" & cell & """" & crit
                If Not Evaluate(expression) Then
                    match = False
                End If
            Else
                ' no eval type
                If Not UCase(cell) Like UCase(crit) Then  ' value2?
                    match = False
                End If
            End If
            If match = False Then
                irc = irc + 1
            End If
        Next arg
        If match Then
            matchCount = matchCount + 1
            matches(matchCount) = rng(i)
        End If
    Next i

    'remove duplicates
    Dim singleMatches() As String
    ReDim singleMatches(matchCount)
    Dim j As Integer
    Dim n As Integer
    Dim term As String
    Dim singleCount As Integer
    Dim break As Boolean
    For i = 1 To matchCount
        break = False
        term = matches(i)
        'find in exsting ones
        For j = 1 To matchCount
            If singleMatches(j) = term Then
                ' already present
                break = True
                Exit For
            End If
                
        Next j
        If Not break Then
            singleCount = singleCount + 1
            singleMatches(singleCount) = term
        End If
    Next i
    
    ' concatenate
    Dim res As String
    For i = 1 To singleCount - 1
        res = res & singleMatches(i) & oddelovac
        
    Next i
    res = res & singleMatches(singleCount)
    Application.ScreenUpdating = True
    LISTIFS = res
    
    

End Function

